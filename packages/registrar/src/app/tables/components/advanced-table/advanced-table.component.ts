import {ChangeDetectorRef, Component, ElementRef, Inject, InjectionToken, Input, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import 'datatables';
import {TranslatePipe, TranslateService} from '@ngx-translate/core';
import {template, at} from 'lodash';
import {ActivatedRoute} from '@angular/router';
import {TableConfiguration} from './advanced-table.interfaces';
import {DataServiceQueryParams} from '@themost/client/src/common';
declare var $: any;


class TableTranslationChangeDetector extends ChangeDetectorRef {
    checkNoChanges(): void {
    }

    detach(): void {
    }

    detectChanges(): void {
    }

    markForCheck(): void {
    }

    reattach(): void {
    }

}

export let COLUMN_FORMATTERS = new InjectionToken('column.formatters');

@Component({
    selector: 'app-advanced-table',
    template: `
        <table #table class="dataTable">
        </table>
    `,
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./advanced-table.component.scss']
})
export class AdvancedTableComponent implements OnInit {

    @ViewChild('table') table: ElementRef;
    @Input() config: TableConfiguration;
    @Input() height = 800;
    @Input() showActions = true;
    public readonly translator: TranslatePipe;
    private dataTable: any;
    private lastQueryParams: DataServiceQueryParams;

    constructor(private _context: AngularDataContext,
                @Inject(COLUMN_FORMATTERS) private _columnFormatters: Array<Function>,
                private _activatedRoute: ActivatedRoute,
                private _translateService: TranslateService) {
        // initialize translate pipe
        this.translator = new TranslatePipe(this._translateService, new TableTranslationChangeDetector());
        $.fn.dataTable.ext.classes.sPageButton = 'btn';
        $.fn.dataTable.ext.classes.sPageButtonActive = 'btn btn-light';
    }

    search(text) {
        this.dataTable.search(text).draw();
    }

    fetch() {
        this.dataTable.draw();
    }

    ngOnInit() {
        const self = this;
        if (this.config) {
            // prepare configuration for columns
            const tableColumns = this.config.columns.map( column => {
                const columnDefinition = {
                    data: column.property || column.name,
                    defaultContent: column.hasOwnProperty('defaultContent') ? column.defaultContent : '',
                    title: this.translator.transform(column.title),
                    sortable: column.hasOwnProperty('sortable') ? column.sortable : true,
                };

                if (column.formatter) {
                    const formatter = this._columnFormatters.find( x => {
                       return x.name === column.formatter;
                    });
                    if (formatter && typeof formatter.prototype.render === 'function') {
                        Object.assign(columnDefinition, {
                            render: formatter.prototype.render.bind(column)
                        });
                    }
                }

                return columnDefinition;
            });
            // if show actions dots should be visible as last column
            if (this.showActions) {
                // add text dots columns
                // todo:: add row actions
                tableColumns.push({
                    data: null,
                    defaultContent: '<span class="fa-2x text-dots"></span>',
                    title: '',
                    sortable: false
                });
            }
            // get table element
            const tableElement = $(this.table.nativeElement);
            // initialize data table
            const settings = {
                // set length menu
                lengthMenu: [50, 100, 200, 500],
                // hide length menu
                lengthChange: false,
                // enable search but hide search box (css)
                searching: true,
                // enable data processing
                processing: true,
                // enable getting server side data
                serverSide: true,
                // set continuous scrolling
                scrollCollapse: true,
                scroller: {
                    loadingIndicator: false,
                    displayBuffer: 10
                },
                // set scroll y
                scrollY: this.height,
                // set scroll x
                scrollX: false,
                // enumerate table columns
                columns: tableColumns,
                // define server data callback
                fnServerData: function (sSource, aoData, fnCallback, oSettings) {
                    // get activated route params
                    self._activatedRoute.queryParams.subscribe( queryParams => {
                        // get columns
                        const columns = aoData[1].value;
                        // get order expression
                        const order = aoData[2].value;
                        // get skip records param
                        const skip = aoData[3].value;
                        // get page size param
                        const top = aoData[4].value;
                        // get search value
                        const searchText = aoData[5].value;
                        // get data queryable
                        const q = self._context.model(self.config.model).asQueryable();
                        // apply paging params
                        if (top) {
                            q.take(top).skip(skip);
                        }
                        // apply order params
                        if (order && order.length) {
                            // get order query expression
                            const orderByStr = order.map( expr => {
                                return self.config.columns[expr.column].name + ' ' + expr.dir || 'asc';
                            }).join(',');
                            // set order
                            q.setParam('$orderby', orderByStr);
                        }
                        // configure $select parameter
                        const select = self.config.columns.map( column => {
                           if (column.property) {
                               return column.name + ' as ' + column.property;
                           } else {
                               return column.name;
                           }
                        });
                        q.select.apply(q, select);
                        // configure $filter
                        if (queryParams.$filter && queryParams.$filter.length) {
                            // set route filter
                            q.setParam('$filter', queryParams.$filter).prepare();
                        }
                        // check if query parameters contain filter
                        if (queryParams.$expand && queryParams.$expand.length) {
                            // set route $expand
                            q.setParam('$expand', queryParams.$expand).prepare();
                        }
                        if (searchText && searchText.value && self.config.searchExpression) {
                            // validate search text in double quotes
                            if (/^"(.*?)"$/.test(searchText.value)) {
                                q.setParam('$filter',
                                    template(self.config.searchExpression)({
                                        text: searchText.value.replace(/^"|"$/g, '')
                                    }));
                            } else {
                                // try to split words
                                const words = searchText.value.split(' ');
                                // join words to a single filter
                                const filter = words.map( word => {
                                    return template(self.config.searchExpression)({
                                        text: word
                                    });
                                }).join (' and ');
                                // set filter
                                q.setParam('$filter', filter);
                            }
                        }
                        self.lastQueryParams = q.getParams();
                        q.getList().then( items => {
                            return fnCallback({
                                'recordsTotal': items['@odata.count'],
                                'recordsFiltered': items['@odata.count'],
                                'data': items.value
                            });
                        });
                    });
                },
                language: self.translator.transform('Tables.DataTable')
            };
            this.dataTable = tableElement.DataTable(settings);
        }
    }

}
