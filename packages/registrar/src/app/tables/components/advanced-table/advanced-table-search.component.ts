import {Component, Input, OnInit, ViewChild, ViewEncapsulation} from '@angular/core';
import {AdvancedTableComponent} from './advanced-table.component';

@Component({
    selector: 'app-advanced-table-search',
    template: `
        <div class="s--list-navbar d-none d-sm-block">
            <form class="form-inline p-3 col">
                <div class="form-group col-7">
                    <label class="pr-3" for="searchText" [translate]="'Tables.Search'"></label>
                    <input (keydown)="onSearchKeyDown($event)" spellcheck="false" type="text" id="searchText" name="searchText"
                           class="form-control col text-left" placeholder="{{ 'Tables.SearchPlaceholder' | translate }}" >
                </div>
            </form>
        </div>
    `,
    encapsulation: ViewEncapsulation.None
})

export class AdvancedTableSearchComponent implements OnInit {

    @Input() table: AdvancedTableComponent;

    constructor() {

    }
    ngOnInit(): void {

    }

    onSearchKeyDown(event: any) {
        if (this.table && event.keyCode === 13) {
            this.table.search((<HTMLInputElement>event.target).value);
            return false;
        }
    }

}
