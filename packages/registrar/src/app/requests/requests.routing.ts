import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RequestsHomeComponent} from './components/requests-home/requests-home.component';

const routes: Routes = [
    {
        path: '',
        component: RequestsHomeComponent,
        data: {
            title: 'Requests'
        }
    }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    declarations: []
})
export class RequestsRoutingModule {
}
