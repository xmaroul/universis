import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { StudentsHomeComponent } from './components/students-home/students-home.component';
import {StudentsRoutingModule} from './students.routing';
import {StudentsSharedModule} from './students.shared';
import { StudentsTableComponent } from './components/students-table/students-table.component';
import {TablesModule} from '../tables/tables.module';
import {TranslateModule} from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    StudentsSharedModule,
      TablesModule,
    StudentsRoutingModule
  ],
  declarations: [StudentsHomeComponent, StudentsTableComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class StudentsModule { }
