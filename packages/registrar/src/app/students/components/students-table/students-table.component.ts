import { Component, OnInit } from '@angular/core';
import * as STUDENTS_LIST_CONFIG from './students-table.config.json';

@Component({
  selector: 'app-students-table',
  templateUrl: './students-table.component.html',
  styles: []
})
export class StudentsTableComponent implements OnInit {

  public readonly config = STUDENTS_LIST_CONFIG;

  constructor() { }

  ngOnInit() {
  }

}
